1. Setting up Environment.
    - Understanding overall theory of MVC.
	- Install CakePHP and Create Project Directory (Follow Initial steps of 20min Tutorial in Documentation https://book.cakephp.org/3.0/en/tutorials-and-examples/cms/installation.html)
	- Make sure Server is working (execute command in project directory -> "bin/cake server" Should start CakePHP Homepage on localhost).
	- Install MySQL Database and Setup Credentials. (same as in /cms/config/app.php )
	- Connect MySQL Database to CakePHP server, CakePHP homepage on localhost should detect MySQL Database. (enable extensions if needed https://stackoverflow.com/questions/29403450/cakephp-3-is-not-able-to-connect-to-the-database-due-to-a-missing-php-extension)
	- Install Visual Studio.
	

2. Getting familiar to MVC	
	- Start with 20Min Tutorial in CakePHP Documentation ( https://book.cakephp.org/3.0/en/tutorials-and-examples.html )
	- Follow everything up to Authentication Step.


3. Adding Additional Features to Tutorial
	- Add a comments feature for existing article project.
	- Display Articles list in Descending order based on Number of Comments i.e. Article with Max Comments on Top.


4. Understanding significance of Bake.
	- (Comment) Table can be accessed using TableRegistry, Loading Model or Using Model funtions obtained by Baking (comment) table similar to Articles table in tutorial.
	- i. Use "TableRegistry" to access Table in model file of its own.
	   ii. Use "Load Model" to access other tables in Current Model file. (eg. Accessing Users table in articles model)
	   iii. Model Functions (using Contains) in Controller if join exists (Only for high level association) or Using Model funtions using query builder if join doesnt exists
	- Convention to follow before Baking a Table
		- Table names should be always in plural form. (example, users)
		- column name should include underscore ' _ ' wherever blank-space is needed (example, user_id)
	- (If not already done) Try Creating all CURD Functions for comments table just by using "Bake" on all tables including comments.
	- Baking a (comment) table should automatically provide its  Model, View and Controller in respective directories.
	- Baking also sets Foreign Key associations, Primary key, etc automatically. 
	

5. Git
	- Register on Gitlab and Install Git on desktop.
	- Create a master branch for Existing Articles project.
	- Perform and understand add . / commit / push operations, Simultaneously Adding new Features to Articles projects. (on appropriate branch)
	- Commit changes to Git with appropriate commit message.
	- "git checkout -b" [branch name] (To Create a new branch and switch to it)


6. Adding New Features to Existing Project.
    ('git commit' locally as features are being added, onwards)
	- Only Logged in user should be able to comment with Respective name.
	- Display username of logged in user next to Comment box. (example, "Commenting as Karan")
	- Accessing session/Auth variable through Views. (For getting username)
	- Link Comment list (Onclick) back to Article posted. (Should be done if baked properly)
	- Enable Multiple Tags Selection for Articles.
	- Authorize Edit Article option only to user who created the article.
	- Display List of Articles only if created by User.
	- By Default deny all routes (features) and enable only as required.


* Setting Up Manager on LocalHost
    - Clone Repo 'manager' and 'yocket-manager-vendor', execute "git clone [URL]"
    - Switch to development, execute "git checkout development" (Current dev branch)
    - Pull latest, execute "git pull" 
    - Copy 'yocket-manager-vendor' folder inside manager change name to 'vendor'
    - Change Database connection details in manager/config/app.php as required, Test command "bin/cake server" 
    - Import yocket.sql ( https://drive.google.com/file/d/1jVddInX-V5tY3ylNZsLZMtJO0M-a-11g/view )
      file in phpmyadmin/mysql db , change following config in php.ini if limit error occurs

        - upload_max_filesize 120M //file size
        - post_max_size 120M
        - max_execution_time 200
        - max_input_time 200