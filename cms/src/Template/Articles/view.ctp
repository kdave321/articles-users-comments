<!-- File: src/Template/Articles/view.ctp -->

<h1><?= h($article->title) ?></h1>
<p><?= h($article->body) ?></p>
<p><small>Created: <?= $article->created->format(DATE_RFC850) ?></small></p>
<p><?= $this->Html->link('Edit', ['action' => 'edit', $article->slug]) ?></p>
<p><b>Tags:</b> <?= h($article->tag_string) ?></p>
<h2>Comments</h2>
<?php
    echo $this->Form->create("comments",array('url'=>'/comments/add'));
    echo $this->Form->input('article_id', ['type' => 'hidden', 'value' => $article->id]);
    echo $this->Form->input('body', ['rows' => '3']);
    echo $this->Form->button('Submit Comment');
    echo 'Comment as '; 
    echo $auth->user('email'); //Works if set in controller
    echo $this->request->session()->read('Auth.User.email'); // Direct access to session without setting.
    echo $this->Form->end();	
?>
<table>
<?php foreach ($comments as $comment):
	if($comment->article_id == $article->id){?>	 	
	<tr><td><?= h($comment->name) ?> : <?= h($comment->body) ?></td></tr>

<?php }endforeach; ?>
</table>
